CHALLENGE:

Two html pages that communicates the current time on a location using WebSockets. 

Relevant links:

- https://gist.github.com/laurentdebricon/1769458
- http://en.wikipedia.org/wiki/Haversine_formula

WebSocket Server - can be hosted locally (localhost):
1. Using python existing WebSocket library (ie., Tornado), write a WebSocket server.
2. Any messages received from html page1, relay it to the WebSocket client html page2.

HTML PAGE 1 (Time and Location Sender):

1. Divide the screen, top part is a map (you can use the free google map api) and a bottom footer to display some information.
2. Connects as a WebSocket client to the WebSocket server.
3. As user clicks anywhere on the map, display the current time of that location in the footer using angularjs.
Also send the latitute, longitude and current time to the WebSocket server.

HTML PAGE 2 (Time and Location Receiver):

1. This page connects as a WebSocket client and only receives messages from the WebSocket server.
2. Any messages received is displayed on the page.
