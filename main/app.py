"""
Created on May 24, 2016

@author: jvazquez
"""
import json
import logging
import os
import uuid

from datetime import datetime

import logconfig
import pytz

from haversine import haversine
from tornado import gen, websocket, web, ioloop
from tzwhere import tzwhere

STATIC_FOLDER = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                             "../static")
TIMEZONES_DB = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                            "timezones.txt")
LOG_CONFIGURATION = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                                 "../configuration/logging.json")

logger = logging.getLogger(__name__)
logconfig.from_json(LOG_CONFIGURATION)

cl = []


class HaversineCalculatorHandler(web.RequestHandler):
    # Too slow
    def initialize(self, timezone_db):
        self.timezone_db = timezone_db

    @web.asynchronous
    @gen.engine
    def post(self):
        received_latitude = float(self.get_argument("latitude"))
        received_longitude = float(self.get_argument("longitude"))
        user_data = (received_latitude, received_longitude)
        response = yield gen.Task(self.do_haversine_calc, user_data)
        self.write('<li>{}</li>'.format(response))
        self.finish()

    def do_haversine_calc(self, tuple_data, callback):
        result_buffer = []
        for db_data in self.timezone_db:
            latitude_db = float(db_data[:2][0])
            longitude_db = float(db_data[:2][1])
            prepared_data = (latitude_db, longitude_db)
            try:
                result = haversine(prepared_data, tuple_data)
                if int(result) < 100:
                    result_buffer.append(result)
                    logger.info("You are in {}".format(db_data[2]))
            except Exception:
                logger.info("Haversine error")
                logger.exception("Got haversine error?")

        return callback(tuple_data)


class HaversineLibCalculatorHandler(web.RequestHandler):
    def initialize(self, tz_lib):
        self.tz_lib = tz_lib

    @web.asynchronous
    def post(self):
        received_latitude = float(self.get_argument("latitude"))
        received_longitude = float(self.get_argument("longitude"))
        try:
            timezone = self.tz_lib.tzNameAt(received_latitude,
                                            received_longitude)
            logger.info("The obtained timezone {}".format(timezone))
            tz = pytz.timezone(timezone)
            provided_zone = datetime.now(tz)
            self.write(json.dumps({"timezone_name": timezone,
                                   "timezone_time": provided_zone.isoformat()}))
        except Exception:
            logger.exception("Error while trying to convert the timezone")
            self.write(json.dumps({"timezone_name": "Sorry, that area can't"
                                   "be found in our db", "zone_time": ""}))
        self.finish()


class WebAppHandler(web.RequestHandler):
    def get(self):
        self.render("{}/index.html".format(STATIC_FOLDER))


class ClientPageHandler(web.RequestHandler):
    def get(self):
        self.render("{}/client.html".format(STATIC_FOLDER))


class SocketHandler(websocket.WebSocketHandler):
    def check_origin(self, origin):
        return True

    def on_message(self, message):
        # self.write_message(u"You said: " + message)
        for client in cl:
            client.write_message(message)

    def open(self):
        self.id = uuid.uuid4()
        if self not in cl:
            cl.append(self)

    def on_close(self):
        if self in cl:
            cl.remove(self)


def get_db():
    db = []
    with open(TIMEZONES_DB, "r") as stream:
        timezone_db = stream.read().splitlines()

    for row in timezone_db:
        elements = row.split()
        db.append((float(elements[0]), float(elements[1]), elements[2]))
    return db


def prepare_tzwhere():
    return tzwhere.tzwhere()


app = web.Application([
    (r'/', WebAppHandler),
    (r'/websocket', ClientPageHandler),
    (r'/api', HaversineCalculatorHandler, {"timezone_db": get_db()}),
    (r'/api-haversine', HaversineLibCalculatorHandler,
     {"tz_lib": prepare_tzwhere()}),
    (r'/ws', SocketHandler),
    (r'/(favicon.ico)', web.StaticFileHandler, {'path': '../'}),
    (r"/static/(.*)", web.StaticFileHandler,
     {"path": "{}".format(STATIC_FOLDER)}),
])

if __name__ == '__main__':
    port = 8888
    logger.info("We will run at port {}".format(port))
    app.listen(port)
    ioloop.IOLoop.instance().start()
