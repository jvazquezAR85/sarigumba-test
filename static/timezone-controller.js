angular.module('HelloUserApp', [])
      .controller('HelloUserController', function($scope) {
          $scope.greeting = "Timezone indicator";
          $scope.timezone_time = "";
          $scope.timezone_name = "";
          $scope.customParams = {};
          $scope.updateTz = function (data) {
        	  var received = JSON.parse(data);
        	  $scope.timezone_name = received.timezone_name;
        	  $scope.timezone_time = received.timezone_time;
          }
      }
);
